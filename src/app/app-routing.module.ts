import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PeerComponent } from './peer/peer.component';

const routes: Routes = [
  { path: 'peer', component: PeerComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
