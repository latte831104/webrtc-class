import express = require('express');
// import * as cookieParser from 'cookie-parser';
// import * as logger from 'morgan';
import * as path from 'path';

import { NextFunction, Request, Response, Router } from 'express';
const http = require('http');

// Get our API routes
// const api = require('./server/routes/index.api');

const app = express();
const project = 'webrtc-class'
// const cors = require('cors')

// import { getLocalIpHost } from './dist/out-tsc/lib/functions/node/ip'
// Parsers for POST data
// app.use(cors())


// view engine setup
app.set('views', path.join(__dirname, 'public')); // Server Side Render Engine
app.set('view engine', 'ejs');

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
// app.use(cookieParser());
// Point to Angular App's folder
// Point static path to dist
app.use(express.static(path.join(__dirname, path.extname(__filename) == '.ts' ? `dist/${project}` : `../${project}`)));

// Set our api routes
// app.use('/api', api);
app.use('/external-link/*', (req, res) => {
  // res.send(req.query)
  res.redirect(`${req.query['redirect_url']}?${req.query['token_payload']}`);
})

// Catch all other routes and return the index file
app.get('*', (req, res) => {
  if (req.path.includes('auth')) {
    if (Object.keys(req.query).length !== 0 && req.query['_from'].length !== 1) res.sendFile(path.join(__dirname, path.extname(__filename) == '.ts' ? `dist/${project}/index.html` : `../${project}/index.html`));
    else res.send(`
    <h1>請由以下兩個網址重新訪問</h1>\n
    <h2><a href="/web">網頁端 </a></h2>\n
    <h2><a href="/mobile">移動端 </a></h2>\n`)
  }
  else if (req.path.includes('api') || req.path.includes('external-link')) {
    // res.send(`http://${req.headers.host}${req.path}`);
    // console.log(req.path)
  }
  else
    res.sendFile(path.join(__dirname, path.extname(__filename) == '.ts' ? `dist/${project}/index.html` : `../${project}/index.html`));
});

// catch 404 and forward to error handler
app.use(function (req: Request, res: Response, next: NextFunction) {
  return res.status(404).send({
    status: 404,
    name: 'Route Not Found',
    message: `Route ${req.url} not found , please check your application`
  });
});

// error handler
app.use(function (err: any, req: Request, res: Response, next: NextFunction) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  // render the error page
  // TODO Check this
  res.status(err.status || 500);
  res.send('error');
});

module.exports = app;


