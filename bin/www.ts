import * as http from 'http';
import { Request, Response, Application } from 'express';
import * as path from 'path';
// import { getLocalIpHost } from '../lib/functions/node/ip';
import moment = require('moment');
// import * as _ from 'lodash'
const PeerServer = require('peer').PeerServer;
const ExpressPeerServer = require('peer').ExpressPeerServer;
import express from 'express';

export const runningMode = process.env.RUNNING_STATUS ? 'prod' : `dev`;
const fs = require('fs');
var mycenaObj = {
  peerjs: {
    peerList: [],
  },
  socketIO: {
    roomList: [],
    socketList: [],
  }
}
// process.env.RUNNING_STATUS設定於app.yaml
// dev: local開發版本, prod: GAE雲端正式版

const app: Application = require('../app');
// process.env.PORT ? console.log(`It's running in Could Server. (./bin/www.js; line:6)`) : console.log(`It's running in Local Server. (./bin/www.ts; line:6)`)
console.log(
  `= = = = = = = = = = = = = = = = = = = = = = = =\n`,
  path.extname(__filename) == '.ts' ?
    `Server is running by "npm run start:watch".(for local develop)\n(${__filename})\n` :
    `Server is running by "npm start".(for GAE)\n(${__filename})\n`,
  `## /bin/www.ts:8;\n= = = = = = = = = = = = = = = = = = = = = = = =\n`,
)

const port = process.env.PORT || '8082';
app.set('port', port);

export const server = http.createServer(app).listen(port, (req: Request, res: Response) => {
});

// * Listen on provided port, on all network interfaces.
// server.listen(port);

// * Event listener for HTTP server "error" event.
server.on('error', (error: any) => {
  if (error.syscall !== 'listen') {
    throw error;
  }
  const bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port;
  // * handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
});

// * Event listener for HTTP server "listening" event.
server.on('listening', () => {
  const addr = server.address();
  const bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port;
  let _time = () => {
    let now = moment(), weak = ['日', '一', '二', '三', '四', '五', '六']
    // return `${now.format('YYYY/MM/DD')}(${weak[now.format('d')]}) ${now.format('HH:mm:ss')}`
    return now.format('YYYY/MM/DD(ddd) HH:mm:ss')
  }


  console.log(
    `Start Time: ${_time()}\nNode Server run: http://localhost:${port}\n`
  )
});

// console.log(server)
// const peerServer = http.createServer(app)
export const peerserver = PeerServer({ port: Number(port) + 1, path: '/' });
peerserver.on('connection', function (id) {
  console.log(id)
});
peerserver.on('disconnected', function (id) {
  console.log(1, id)
});



const io = require('socket.io')(server);
io.sockets.on('error', e => console.log(e));
io.sockets.on('connection', (socket) => {
  socket.broadcast.emit('user connected');
  socket.emit('sdp', 'TEST 2222222')

  socket.on('addPeer', (id) => {
    mycenaObj['peerjs']['peerList'].push(id)
    // console.log(mycenaObj)
  })
  socket.on('delPeer', (id) => {
    mycenaObj['peerjs']['peerList'] = mycenaObj['peerjs']['peerList'].filter(peer => peer !== id)
    console.log(id, mycenaObj)
  })
  socket.on('disconnect', () => {
    io.emit('User left')
  });
});

const room = io.of('/room')
room.on('connection', socket => {

  socket.on('ready', req => {
    Object.values(req).map(room => {
      socket.join(room)
    })
    // socket.join(req['chatRoom'])
    console.log('ready:\n', req)
  })

  socket.on('send', req => {
    // console.log('send:\n', req)
    socket.broadcast.to(req.room).emit('message', {
      message: req.message,
      author: req.author
    });
  })
  socket.on('signal', req => {
    console.log('signal:\n', req)
    socket.broadcast.to(req.room).emit('signalingMessage', {
      type: req.type,
      message: req.message
    });
  })
})





// app.use('/peerjs', peersjs);
